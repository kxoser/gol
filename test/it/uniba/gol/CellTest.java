package it.uniba.gol;

import static org.junit.Assert.*;

import org.junit.Test;

public class CellTest {

	@Test
	public void cellShouldBeAlive() throws Exception {
		Cell cell = new Cell(0, 0, true); 
		
		assertTrue(cell.isAlive());
	}

	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnX() throws Exception {
		new Cell(-1, 0, true); 
	}
	
	@Test(expected = CustomLifeException.class)
	public void cellShouldRaiseExceptionOnY() throws Exception {
		new Cell(0, -1, true); 
	}
	
	@Test
	public void cellShouldBeDead() throws Exception {
		Cell cell = new Cell(0, 0, true); 
		cell.setAlive(false);
		
		assertFalse(cell.isAlive());
	}
	
	@Test
	public void cellShouldReturnX() throws Exception {
		Cell cell = new Cell(5, 0, true); 
		
		assertEquals(5, cell.getX());
	}
	
	@Test
	public void cellShouldReturnY() throws Exception {
		Cell cell = new Cell(0, 5, true); 
		
		assertEquals(5, cell.getY());
	}
	
	@Test
	public void cellShouldHaveAliveNeighbords() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertEquals(3, cell[1][1].getNumberOfAliveNeighbords());
	}
	
	@Test
	public void aliveCellShouldSurviveWithThreeNeighbors() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertTrue(cell[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellShouldSurviveWithTwoNeighbors() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,false);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertTrue(cell[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellShouldNotSurvive() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,false);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willSurvive());
	}
	
	@Test
	public void deadCellShouldNotSurvive() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,false);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willSurvive());
	}
	
	@Test
	public void aliveCellWithMoreThanThreeNeighborsShouldDie() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertTrue(cell[1][1].willDie());
	}
	
	@Test
	public void aliveCellWithLessThanTwoNeighborsShouldDie() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,true);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,false);
		cell[2][1] = new Cell(2,1,false);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertTrue(cell[1][1].willDie());
	}
	
	@Test
	public void aliveCellShouldNotDie() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willDie());
	}
	
	
	
	@Test
	public void deadCellShouldNotDie() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willDie());
	}
	
	@Test
	public void deadCellShouldRevive() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertTrue(cell[1][1].willRevive());
	}
	
	
	@Test
	public void deadCellWithLessThanTwoNeighborsShouldNotRevive() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,false);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,false);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willRevive());
	}
	
	@Test
	public void aliveCellShouldNotRevive() throws Exception {
		Cell[][] cell = new Cell[3][3]; 
		cell[0][0] = new Cell(0,0,false);
		cell[0][1] = new Cell(0,1,true);
		cell[0][2] = new Cell(0,2,false);
		
		cell[1][0] = new Cell(1,0,false);
		cell[1][1] = new Cell(1,1,true);
		cell[1][2] = new Cell(1,2,false);
		
		cell[2][0] = new Cell(2,0,true);
		cell[2][1] = new Cell(2,1,true);
		cell[2][2] = new Cell(2,2,false);
		
		cell[1][1].setNumberOfAliveNeighbors(cell);
		
		assertFalse(cell[1][1].willRevive());
	}
	
	@Test
	public void cellShouldNotBeNeighborsOnX() throws Exception {
		Cell cell1 = new Cell(0,0,true); 
		Cell cell2 = new Cell(2,0,true); 
		
		assertFalse(cell1.isNeighboard(cell2));
	}
	
	@Test
	public void cellShouldNotBeNeighborsOnY() throws Exception {
		Cell cell1 = new Cell(0,0,true); 
		Cell cell2 = new Cell(0,2,true); 
		
		assertFalse(cell1.isNeighboard(cell2));
	}
}
